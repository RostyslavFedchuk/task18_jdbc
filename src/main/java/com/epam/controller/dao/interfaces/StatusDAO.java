package com.epam.controller.dao.interfaces;

import com.epam.model.tables.Status;

public interface StatusDAO extends GeneralDAO<Status> {
}
