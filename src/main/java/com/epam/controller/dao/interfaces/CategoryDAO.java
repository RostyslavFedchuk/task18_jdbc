package com.epam.controller.dao.interfaces;

import com.epam.model.tables.Category;

public interface CategoryDAO extends GeneralDAO<Category> {
}
