package com.epam.controller.dao.interfaces;

import com.epam.model.tables.Order;

public interface OrderDAO extends GeneralDAO<Order>{
}
