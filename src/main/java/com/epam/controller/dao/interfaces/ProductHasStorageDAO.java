package com.epam.controller.dao.interfaces;

import com.epam.model.tables.ProductHasStorage;

public interface ProductHasStorageDAO extends GeneralDAO<ProductHasStorage> {
}
