package com.epam.controller.dao.interfaces;

import com.epam.model.tables.Toy;

public interface ToyDAO extends GeneralDAO<Toy> {
}
