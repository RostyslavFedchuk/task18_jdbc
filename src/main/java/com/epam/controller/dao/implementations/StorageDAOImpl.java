package com.epam.controller.dao.implementations;

import com.epam.controller.Transformer;
import com.epam.controller.dao.interfaces.ProductHasStorageDAO;
import com.epam.controller.dao.interfaces.StorageDAO;
import com.epam.model.ConnectionManager;
import com.epam.model.tables.ProductHasStorage;
import com.epam.model.tables.Storage;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StorageDAOImpl implements StorageDAO {
    private static final String FIND_ALL;
    private static final String DELETE;
    private static final String CREATE;
    private static final String UPDATE;

    static {
        FIND_ALL = bundle.getString("FIND_ALL_Storage");
        DELETE = bundle.getString("DELETE_Storage");
        CREATE = bundle.getString("CREATE_Storage");
        UPDATE = bundle.getString("UPDATE_Storage");
    }

    @Override
    public List<Storage> findAll() throws SQLException {
        List<Storage> storages = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    storages.add((Storage) new Transformer<>(Storage.class).getInstance(resultSet));
                }
            }
        }
        return storages;
    }

    @Override
    public int create(Storage object) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(CREATE)) {
            statement.setInt(1, object.getId());
            statement.setString(2, object.getName());
            statement.setString(3, object.getAddress());
            return statement.executeUpdate();
        }
    }

    @Override
    public int update(Storage object) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(UPDATE)) {
            statement.setString(2, object.getName());
            statement.setString(1, object.getAddress());
            return statement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(DELETE)) {
            statement.setInt(1, id);
            return statement.executeUpdate();
        }
    }

    @Override
    public int deleteStorage(Integer toDelete, Integer toPaste) throws SQLException {
        int count = 0;
        Connection connection = ConnectionManager.getConnection();
        try{
            List<ProductHasStorage> storagesByIt = new ProductHasStorageDAOImpl().findAll().stream()
                    .filter(s -> s.getStorageId().equals(toDelete)).collect(Collectors.toList());
            connection.setAutoCommit(false);
            for(ProductHasStorage phs: storagesByIt){
                phs.setStorageId(toPaste);
                count += new ProductHasStorageDAOImpl().update(phs);
            }
            count += delete(toDelete);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.setAutoCommit(true);
        }
        return count;
    }
}
