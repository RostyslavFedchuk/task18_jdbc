package com.epam.controller.dao.implementations;

import com.epam.controller.Transformer;
import com.epam.controller.dao.interfaces.UserDAO;
import com.epam.model.ConnectionManager;
import com.epam.model.tables.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAOImpl implements UserDAO {
    private static final String FIND_ALL;
    private static final String DELETE;
    private static final String CREATE;
    private static final String UPDATE;

    static {
        FIND_ALL = bundle.getString("FIND_ALL_User");
        DELETE = bundle.getString("DELETE_User");
        CREATE = bundle.getString("CREATE_User");
        UPDATE = bundle.getString("UPDATE_User");
    }

    @Override
    public List<User> findAll() throws SQLException {
        List<User> users = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    users.add((User) new Transformer<>(User.class).getInstance(resultSet));
                }
            }
        }
        return users;
    }

    @Override
    public int create(User object) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(CREATE)) {
            statement.setInt(1, object.getId());
            statement.setString(2, object.getLogin());
            statement.setString(3, object.getName());
            statement.setString(4, object.getSurname());
            statement.setString(5, object.getGender());
            statement.setString(6, object.getEmail());
            statement.setString(7, object.getPhoneNumber());
            statement.setString(8, object.getAddress());
            statement.setInt(9, object.getUserPasswordId());
            return statement.executeUpdate();
        }
    }

    @Override
    public int update(User object) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(UPDATE)) {
            statement.setString(1, object.getEmail());
            statement.setString(2, object.getPhoneNumber());
            statement.setString(3, object.getAddress());
            statement.setString(4, object.getLogin());
            return statement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(DELETE)) {
            statement.setInt(1, id);
            return statement.executeUpdate();
        }
    }
}
