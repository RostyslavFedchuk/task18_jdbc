package com.epam.controller.dao.implementations;

import com.epam.controller.Transformer;
import com.epam.controller.dao.interfaces.CategoryDAO;
import com.epam.model.ConnectionManager;
import com.epam.model.tables.Category;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CategoryDAOImpl implements CategoryDAO {
    private static final String FIND_ALL;
    private static final String DELETE;
    private static final String CREATE;
    private static final String UPDATE;

    static {
        FIND_ALL = bundle.getString("FIND_ALL_Category");
        DELETE = bundle.getString("DELETE_Category");
        CREATE = bundle.getString("CREATE_Category");
        UPDATE = bundle.getString("UPDATE_Category");
    }

    @Override
    public List<Category> findAll() throws SQLException {
        List<Category> categories = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try(Statement statement = connection.createStatement()){
            try(ResultSet resultSet = statement.executeQuery(FIND_ALL)){
                while (resultSet.next()){
                    categories.add((Category) new Transformer<>(Category.class).getInstance(resultSet));
                }
            }
        }
        connection.close();
        return categories;
    }

    @Override
    public int create(Category object) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement statement = connection.prepareStatement(CREATE)){
            statement.setInt(1,object.getId());
            statement.setString(2,object.getName());
            statement.setString(3,object.getDescription());
            return statement.executeUpdate();
        }
    }

    @Override
    public int update(Category object) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement statement = connection.prepareStatement(UPDATE)){
            statement.setString(1,object.getDescription());
            statement.setString(2,object.getName());
            statement.setInt(3,object.getId());
            return statement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement statement = connection.prepareStatement(DELETE)){
            statement.setInt(1,id);
            return statement.executeUpdate();
        }
    }
}
