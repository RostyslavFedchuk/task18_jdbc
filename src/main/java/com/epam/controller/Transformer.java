package com.epam.controller;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Date;

import com.epam.model.annotations.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Transformer<T>  {
    private static final Logger LOGGER = LogManager.getLogger(Transformer.class);
    private final Class<T> clas;

    public Transformer(Class<T> clas) {
        this.clas = clas;
    }

    public Object getInstance(ResultSet result) throws SQLException {
        Object object = null;
        try {
            object = getObject(result);
        } catch (IllegalAccessException e) {
            LOGGER.warn("IllegalAccessException\n");
        } catch (InstantiationException e) {
            LOGGER.warn("InstantiationException\n");
        } catch (NoSuchMethodException e) {
            LOGGER.warn("NoSuchMethodException\n");
        } catch (InvocationTargetException e) {
            LOGGER.warn("InvocationTargetException\n");
        }
        return object;
    }


    private Object getObject(ResultSet result)
            throws NoSuchMethodException, IllegalAccessException, InvocationTargetException,
            InstantiationException, SQLException {
        Object object = clas.getConstructor().newInstance();
        if (clas.isAnnotationPresent(Table.class)) {
            Field[] fields = clas.getDeclaredFields();
            for (Field field : fields) {
                if (field.isAnnotationPresent(Column.class)) {
                    Column column = field.getAnnotation(Column.class);
                    field.setAccessible(true);
                    if (field.getType() == String.class) {
                        field.set(object, result.getString(column.name()));
                    } else if (field.getType() == Integer.class) {
                        field.set(object, result.getInt(column.name()));
                    } else if (field.getType() == Double.class) {
                        field.set(object, result.getDouble(column.name()));
                    } else if (field.getType() == Short.class) {
                        field.set(object, result.getShort(column.name()));
                    } else if (field.getType() == Date.class) {
                        field.set(object, result.getDate(column.name()));
                    } else if (field.getType() == Time.class) {
                        field.set(object, result.getTime(column.name()));
                    }
                }
            }
        }
        return object;
    }
}
