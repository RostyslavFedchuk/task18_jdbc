package com.epam.model.tables;

import com.epam.model.annotations.*;

@Table(name = "User_password")
public class Password {
    @PrimaryKey
    @Column(name = "id")
    private Integer id;
    @Column(name = "password")
    private String value;

    public Password(){}

    public Password(Integer id, String password){
        this.id = id;
        value = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getColumns(){
        return String.format("%-7s %-20s", "id", "value");
    }

    @Override
    public String toString() {
        return String.format("%-7d %-20s", id, value);
    }
}
