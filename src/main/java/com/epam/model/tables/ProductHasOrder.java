package com.epam.model.tables;

import com.epam.model.annotations.*;

@Table(name = "Product_has_Order")
public class ProductHasOrder {
    @PrimaryKey
    @Column(name = "id")
    private Integer id;
    @Column(name = "product_id")
    private Integer productId;
    @Column(name = "order_id")
    private Integer orderId;

    public ProductHasOrder(){}

    public ProductHasOrder(Integer id, Integer productId, Integer orderId) {
        this.id = id;
        this.productId = productId;
        this.orderId = orderId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getColumns(){
        return String.format("%-7s %-7s %-7s", "id", "productId", "orderId");
    }

    @Override
    public String toString() {
        return String.format("%-7d %-7d %-7d", id, productId, orderId);
    }
}
