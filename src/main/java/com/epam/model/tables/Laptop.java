package com.epam.model.tables;

import com.epam.model.annotations.*;

@Table(name = "Laptop")
public class Laptop {
    @PrimaryKey
    @Column(name = "bar_code")
    private Integer barCode;
    @Column(name = "name")
    private String name;
    @Column(name = "model")
    private String model;
    @Column(name = "speed")
    private Short speed;
    @Column(name = "ram")
    private Short ram;
    @Column(name = "price")
    private Double price;
    @Column(name = "screen")
    private Short screen;

    public Laptop() {
    }

    public Laptop(Integer barCode, String name, String model,
                  Short speed, Short ram, Double price, Short screen) {
        this.barCode = barCode;
        this.name = name;
        this.model = model;
        this.speed = speed;
        this.ram = ram;
        this.price = price;
        this.screen = screen;
    }

    public Integer getBarCode() {
        return barCode;
    }

    public void setBarCode(Integer barCode) {
        this.barCode = barCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Short getSpeed() {
        return speed;
    }

    public void setSpeed(Short speed) {
        this.speed = speed;
    }

    public Short getRam() {
        return ram;
    }

    public void setRam(Short ram) {
        this.ram = ram;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Short getScreen() {
        return screen;
    }

    public void setScreen(Short screen) {
        this.screen = screen;
    }

    public String getColumns(){
        return String.format("%-7s %-15s %-30s %-10s %-5s %-7.2s %-5s",
                "barCode", "name", "model", "speed", "ram", "price", "screen");
    }

    @Override
    public String toString() {
        return String.format("%-7d %-15s %-30s %-10d %-5d %-7.2f %-5d",
                barCode, name, model, speed, ram, price, screen);
    }
}
