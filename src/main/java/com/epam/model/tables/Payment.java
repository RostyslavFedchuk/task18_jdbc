package com.epam.model.tables;

import com.epam.model.annotations.Column;
import com.epam.model.annotations.PrimaryKey;
import com.epam.model.annotations.Table;

@Table(name = "PaymentDAO")
public class Payment {
    @PrimaryKey
    @Column(name = "id")
    private Integer id;
    @Column(name = "credit_card_number")
    private String creditCardNumber;
    @Column(name = "details")
    private String details;

    public Payment() {
    }

    public Payment(Integer id, String credit_card_number, String details) {
        this.id = id;
        this.creditCardNumber = credit_card_number;
        this.details = details;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCredit_card_number() {
        return creditCardNumber;
    }

    public void setCredit_card_number(String credit_card_number) {
        this.creditCardNumber = credit_card_number;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getColumns(){
        return String.format("%-7s %-20s %-20s", "id", "creditCardNumber", "details");
    }

    @Override
    public String toString() {
        return String.format("%-7d %-20s %-20s", id, creditCardNumber, details);
    }
}
