package com.epam.model.tables;

import com.epam.model.annotations.Column;
import com.epam.model.annotations.PrimaryKey;
import com.epam.model.annotations.Table;

import java.sql.Time;
import java.sql.Date;

@Table(name = "DeliveryDAOImpl")
public class Delivery {
    @PrimaryKey
    @Column(name = "id")
    private Integer id;
    @Column(name = "date")
    private Date date;
    @Column(name = "time")
    private Time time;
    @Column(name = "price")
    private Double price;
    @Column(name = "details")
    private String details;
    @Column(name = "Order_id")
    private Integer orderId;

    public Delivery(){}

    public Delivery(Integer id, Date date, Time time,
                    Double price, String details, Integer orderId){
        this.id = id;
        this.date = date;
        this.price = price;
        this.details = details;
        this.orderId = orderId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getColumns(){
        return String.format("%-7s %-15s %-10s %-10s %-20s %-5s",
                "id", "date", "time", "price", "details", "orderId");
    }

    @Override
    public String toString() {
        return String.format("%-7d %-15s %-10s %-10f %-20s %-5d",
                id, date, time, price, details, orderId);
    }
}
