package com.epam.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import static java.util.Objects.isNull;

public class ConnectionManager {
    private static final Logger LOGGER = LogManager.getLogger(ConnectionManager.class);
    private static final String URL;
    private static final String USER;
    private static final String PASSWORD;
    private static Connection connection;

    static {
        ResourceBundle bundle = ResourceBundle.getBundle("config");
        URL = bundle.getString("URL");
        USER = bundle.getString("USER");
        PASSWORD = bundle.getString("PASSWORD");
    }

    public static Connection getConnection() {
        if(isNull(connection)){
            try {
                connection = DriverManager.getConnection(URL, USER, PASSWORD);
            } catch (SQLException e) {
                e.printStackTrace();
                LOGGER.error("Cannot connect to the database!!!\n");
            }
        }
        return connection;
    }
}
