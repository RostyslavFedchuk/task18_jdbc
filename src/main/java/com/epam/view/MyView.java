package com.epam.view;

import com.epam.controller.DBStructure;
import com.epam.controller.dao.implementations.*;
import com.epam.controller.dao.interfaces.GeneralDAO;
import com.epam.model.tables.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private static final Logger LOGGER = LogManager.getLogger(MyView.class);
    private static final Scanner SCANNER;
    private static Map<String, String> generalMenu;
    private static Map<String, String> tableList;
    private static Map<String, String> tableMenu;
    private static Map<String, String> laptopMenu;
    private static Map<String, String> storageMenu;
    private static Map<String, Printable> generalMenuMethods;
    private static Map<String, Printable> tableListMethods;
    private static Map<String, Printable> tableMenuMethods;

    static {
        generalMenu = new Menu("menu/general_menu").getMenu();
        tableList = new Menu("menu/table_list").getMenu();
        tableMenu = new Menu("menu/table_methods").getMenu();
        SCANNER = new Scanner(System.in);
        setGeneralMenuMethods();
        setTableListMethods();
    }

    private static void setLaptopMethod() {
        laptopMenu = new LinkedHashMap<>(tableMenu);
        laptopMenu.put("5", "Show laptop prices in range.");
    }

    private static void setStorageMethod() {
        storageMenu = new LinkedHashMap<>(tableMenu);
        storageMenu.put("5", "Delete storage with no deleting product.");
    }

    private static void printMenu(Map<String, String> menu) {
        LOGGER.info("__________________________"
                + "MENU__________________________\n");
        for (Map.Entry entry : menu.entrySet()) {
            LOGGER.info(entry.getKey() + " - " + entry.getValue() + "\n");
        }
        LOGGER.info("____________________________________________________\n");
    }

    private static void setGeneralMenuMethods() {
        generalMenuMethods = new LinkedHashMap<>();
        generalMenuMethods.put("1", MyView::showAllTables);
        generalMenuMethods.put("2", MyView::showAllTablesStructure);
        generalMenuMethods.put("3", MyView::chooseTable);
        generalMenuMethods.put("Q", MyView::quit);
    }

    private static void setTableListMethods() {
        tableListMethods = new LinkedHashMap<>();
        tableListMethods.put("1", MyView::CategoryTableSelected);
        tableListMethods.put("2", MyView::DeliveryTableSelected);
        tableListMethods.put("3", MyView::LaptopTableSelected);
        tableListMethods.put("4", MyView::OrderTableSelected);
        tableListMethods.put("5", MyView::PasswordTableSelected);
        tableListMethods.put("6", MyView::PaymentTableSelected);
        tableListMethods.put("7", MyView::ProductTableSelected);
        tableListMethods.put("8", MyView::ProductHasOrderTableSelected);
        tableListMethods.put("9", MyView::ProductHasStorageTableSelected);
        tableListMethods.put("10", MyView::StatusTableSelected);
        tableListMethods.put("11", MyView::StorageTableSelected);
        tableListMethods.put("12", MyView::ToyTableSelected);
        tableListMethods.put("13", MyView::UserTableSelected);
        tableListMethods.put("Q", MyView::quit);
    }

    private static void setCategoryMenuMethods() {
        tableMenuMethods = new LinkedHashMap<>();
        tableMenuMethods.put("1", () -> showTable(new Category().getColumns(),
                new CategoryDAOImpl(), "TABLE Category"));
        tableMenuMethods.put("2", () -> deleteFromTable(new CategoryDAOImpl()));
        tableMenuMethods.put("3", MyView::updateCategory);
        tableMenuMethods.put("4", MyView::insertIntoCategory);
        tableMenuMethods.put("Q", MyView::quit);
    }

    private static void setDeliveryMenuMethods() {
        tableMenuMethods = new LinkedHashMap<>();
        tableMenuMethods.put("1", () -> showTable(new Delivery().getColumns(),
                new DeliveryDAOImpl(), "TABLE Delivery"));
        tableMenuMethods.put("2", () -> deleteFromTable(new DeliveryDAOImpl()));
        tableMenuMethods.put("3", () -> LOGGER.info("...UNAVAILABLE...\n"));
        tableMenuMethods.put("4", () -> LOGGER.info("...UNAVAILABLE...\n"));
        tableMenuMethods.put("Q", MyView::quit);
    }

    private static void setLaptopMenuMethods() {
        setLaptopMethod();
        tableMenuMethods = new LinkedHashMap<>();
        tableMenuMethods.put("1", () -> showTable(new Laptop().getColumns(),
                new LaptopDAOImpl(), "TABLE Laptop"));
        tableMenuMethods.put("2", () -> deleteFromTable(new LaptopDAOImpl()));
        tableMenuMethods.put("3", () -> LOGGER.info("...UNAVAILABLE...\n"));
        tableMenuMethods.put("4", () -> LOGGER.info("...UNAVAILABLE...\n"));
        tableMenuMethods.put("5", MyView::showByPriceRange);
        tableMenuMethods.put("Q", MyView::quit);
    }

    private static void setOrderMenuMethods() {
        tableMenuMethods = new LinkedHashMap<>();
        tableMenuMethods.put("1", () -> showTable(new Order().getColumns(),
                new OrderDAOImpl(), "TABLE Order"));
        tableMenuMethods.put("2", () -> deleteFromTable(new OrderDAOImpl()));
        tableMenuMethods.put("3", () -> LOGGER.info("...UNAVAILABLE...\n"));
        tableMenuMethods.put("4", () -> LOGGER.info("...UNAVAILABLE...\n"));
        tableMenuMethods.put("Q", MyView::quit);
    }

    private static void setPasswordMenuMethods() {
        tableMenuMethods = new LinkedHashMap<>();
        tableMenuMethods.put("1", () -> showTable(new Password().getColumns(),
                new PasswordDAOImpl(), "TABLE Password"));
        tableMenuMethods.put("2", () -> deleteFromTable(new PasswordDAOImpl()));
        tableMenuMethods.put("3", () -> LOGGER.info("...UNAVAILABLE...\n"));
        tableMenuMethods.put("4", () -> LOGGER.info("...UNAVAILABLE...\n"));
        tableMenuMethods.put("Q", MyView::quit);
    }

    private static void setPaymentMenuMethods() {
        tableMenuMethods = new LinkedHashMap<>();
        tableMenuMethods.put("1", () -> showTable(new Payment().getColumns(),
                new PaymentDAOImpl(), "TABLE Payment"));
        tableMenuMethods.put("2", () -> deleteFromTable(new PaymentDAOImpl()));
        tableMenuMethods.put("3", () -> LOGGER.info("...UNAVAILABLE...\n"));
        tableMenuMethods.put("4", () -> LOGGER.info("...UNAVAILABLE...\n"));
        tableMenuMethods.put("Q", MyView::quit);
    }

    private static void setProductMenuMethods() {
        tableMenuMethods = new LinkedHashMap<>();
        tableMenuMethods.put("1", () -> showTable(new Product().getColumns(),
                new ProductDAOImpl(), "TABLE Product"));
        tableMenuMethods.put("2", () -> deleteFromTable(new ProductDAOImpl()));
        tableMenuMethods.put("3", () -> LOGGER.info("...UNAVAILABLE...\n"));
        tableMenuMethods.put("4", () -> LOGGER.info("...UNAVAILABLE...\n"));
        tableMenuMethods.put("Q", MyView::quit);
    }

    private static void setProductHasOrderMenuMethods() {
        tableMenuMethods = new LinkedHashMap<>();
        tableMenuMethods.put("1", () -> showTable(new ProductHasOrder().getColumns(),
                new ProductHasOrderDAOImpl(), "TABLE Product_has_Order"));
        tableMenuMethods.put("2", () -> deleteFromTable(new ProductHasOrderDAOImpl()));
        tableMenuMethods.put("3", () -> LOGGER.info("...UNAVAILABLE...\n"));
        tableMenuMethods.put("4", () -> LOGGER.info("...UNAVAILABLE...\n"));
        tableMenuMethods.put("Q", MyView::quit);
    }

    private static void setProductHasStorageMenuMethods() {
        tableMenuMethods = new LinkedHashMap<>();
        tableMenuMethods.put("1", () -> showTable(new ProductHasStorage().getColumns(),
                new ProductHasStorageDAOImpl(), "TABLE Product_has_Storage"));
        tableMenuMethods.put("2", () -> deleteFromTable(new ProductHasStorageDAOImpl()));
        tableMenuMethods.put("3", () -> LOGGER.info("...UNAVAILABLE...\n"));
        tableMenuMethods.put("4", () -> LOGGER.info("...UNAVAILABLE...\n"));
        tableMenuMethods.put("Q", MyView::quit);
    }

    private static void setStatusMenuMethods() {
        tableMenuMethods = new LinkedHashMap<>();
        tableMenuMethods.put("1", () -> showTable(new Status().getColumns(),
                new StatusDAOImpl(), "TABLE Status"));
        tableMenuMethods.put("2", () -> deleteFromTable(new StatusDAOImpl()));
        tableMenuMethods.put("3", () -> LOGGER.info("...UNAVAILABLE...\n"));
        tableMenuMethods.put("4", () -> LOGGER.info("...UNAVAILABLE...\n"));
        tableMenuMethods.put("Q", MyView::quit);
    }

    private static void setStorageMenuMethods() {
        setStorageMethod();
        tableMenuMethods = new LinkedHashMap<>();
        tableMenuMethods.put("1", () -> showTable(new Storage().getColumns(),
                new StorageDAOImpl(), "TABLE Storage"));
        tableMenuMethods.put("2", () -> deleteFromTable(new StorageDAOImpl()));
        tableMenuMethods.put("3", () -> LOGGER.info("...UNAVAILABLE...\n"));
        tableMenuMethods.put("4", () -> LOGGER.info("...UNAVAILABLE...\n"));
        tableMenuMethods.put("5", MyView::deleteStorageAndMove);
        tableMenuMethods.put("Q", MyView::quit);
    }

    private static void setToyMenuMethods() {
        tableMenuMethods = new LinkedHashMap<>();
        tableMenuMethods.put("1", () -> showTable(new Toy().getColumns(),
                new ToyDAOImpl(), "TABLE Toy"));
        tableMenuMethods.put("2", () -> deleteFromTable(new ToyDAOImpl()));
        tableMenuMethods.put("3", () -> LOGGER.info("...UNAVAILABLE...\n"));
        tableMenuMethods.put("4", () -> LOGGER.info("...UNAVAILABLE...\n"));
        tableMenuMethods.put("Q", MyView::quit);
    }

    private static void setUserMenuMethods() {
        tableMenuMethods = new LinkedHashMap<>();
        tableMenuMethods.put("1", () -> showTable(new User().getColumns(),
                new UserDAOImpl(), "TABLE User"));
        tableMenuMethods.put("2", () -> deleteFromTable(new UserDAOImpl()));
        tableMenuMethods.put("3", () -> LOGGER.info("...UNAVAILABLE...\n"));
        tableMenuMethods.put("4", () -> LOGGER.info("...UNAVAILABLE...\n"));
        tableMenuMethods.put("Q", MyView::quit);
    }

    private static void showByPriceRange() throws SQLException {
        LOGGER.info("(EXAMPLE : 500;600)Enter a price range for Laptops: ");
        String input = SCANNER.next();
        if (input.matches("[0-9]+;[0-9]+")) {
            String[] range = input.split(";");
            LOGGER.info("TABLE Laptop after ranging:\n" + new Laptop().getColumns() + "\n");
            new LaptopDAOImpl().showByPriceRange(Double.valueOf(range[0]), Double.valueOf(range[1]))
                    .forEach(s -> LOGGER.info(s + "\n"));
        } else {
            LOGGER.warn("Wrong input\n");
        }
    }

    private static void deleteStorageAndMove() throws SQLException {
        LOGGER.info("(EXAMPLE 6;5)Enter an id of Storage to delete, and an id of Storage to paste in: ");
        String input = SCANNER.next();
        if (input.matches("[0-9]+;[0-9]+")) {
            String[] range = input.split(";");
            LOGGER.info(new StorageDAOImpl().deleteStorage(Integer.valueOf(range[0]),
                    Integer.valueOf(range[1])) + " rows were affected!\n");
            showTable(new ProductHasStorage().getColumns(), new ProductHasStorageDAOImpl(),
                    "TABLE ProductHasStorage");
            showTable(new Storage().getColumns(), new StorageDAOImpl(), "TABLE Storage");

        } else {
            LOGGER.warn("Wrong input\n");
        }
    }

    private static void showTable(String columns, GeneralDAO general, String message) throws SQLException {
        LOGGER.info("\n" + message + ":\n" + columns + "\n");
        general.findAll().forEach(s -> LOGGER.info(s + "\n"));
    }

    private static void updateCategory() throws SQLException {
        LOGGER.info("Enter id, name and description:\n");
        String input = SCANNER.nextLine();
        if (input.matches("[0-9]+(\\s[a-zA-Z]+){2}")) {
            String[] range = input.split(" ");
            Category newCategory = new Category(Integer.valueOf(range[0]), range[1], range[2]);
            LOGGER.info(new CategoryDAOImpl().update(newCategory) + " rows were successfully updated!\n");
        } else {
            LOGGER.warn("Wrong input\n");
        }
    }

    private static void deleteFromTable(GeneralDAO general) throws SQLException {
        LOGGER.info("Enter id to delete:\n");
        LOGGER.info(general.delete(SCANNER.nextInt()) + " rows were successfully deleted!\n");
    }

    private static void insertIntoCategory() throws SQLException {
        LOGGER.info("Enter id, name and description to create a category:\n");
        String input = SCANNER.nextLine();
        if (input.matches("[0-9]+(\\s[a-zA-Z]+){2}")) {
            String[] range = input.split(" ");
            Category newCategory = new Category(Integer.valueOf(range[0]), range[1], range[2]);
            new CategoryDAOImpl().create(newCategory);
            LOGGER.info("Row was successfully added!\n");
        } else {
            LOGGER.warn("Wrong input\n");
        }
    }

    private static void CategoryTableSelected() throws SQLException {
        setCategoryMenuMethods();
        showMenu(tableMenu, tableMenuMethods);
    }

    private static void DeliveryTableSelected() throws SQLException {
        setDeliveryMenuMethods();
        showMenu(tableMenu, tableMenuMethods);
    }

    private static void LaptopTableSelected() throws SQLException {
        setLaptopMenuMethods();
        showMenu(laptopMenu, tableMenuMethods);
    }

    private static void OrderTableSelected() throws SQLException {
        setOrderMenuMethods();
        showMenu(tableMenu, tableMenuMethods);
    }

    private static void PasswordTableSelected() throws SQLException {
        setPasswordMenuMethods();
        showMenu(tableMenu, tableMenuMethods);
    }

    private static void PaymentTableSelected() throws SQLException {
        setPaymentMenuMethods();
        showMenu(tableMenu, tableMenuMethods);
    }

    private static void ProductTableSelected() throws SQLException {
        setProductMenuMethods();
        showMenu(tableMenu, tableMenuMethods);
    }

    private static void ProductHasOrderTableSelected() throws SQLException {
        setProductHasOrderMenuMethods();
        showMenu(tableMenu, tableMenuMethods);
    }

    private static void ProductHasStorageTableSelected() throws SQLException {
        setProductHasStorageMenuMethods();
        showMenu(tableMenu, tableMenuMethods);
    }

    private static void StatusTableSelected() throws SQLException {
        setStatusMenuMethods();
        showMenu(tableMenu, tableMenuMethods);
    }

    private static void StorageTableSelected() throws SQLException {
        setStorageMenuMethods();
        showMenu(storageMenu, tableMenuMethods);
    }

    private static void ToyTableSelected() throws SQLException {
        setToyMenuMethods();
        showMenu(tableMenu, tableMenuMethods);
    }

    private static void UserTableSelected() throws SQLException {
        setUserMenuMethods();
        showMenu(tableMenu, tableMenuMethods);
    }

    private static void showAllTables() throws SQLException {
        showTable(new Category().getColumns(), new CategoryDAOImpl(), "TABLE Category");
        showTable(new Delivery().getColumns(), new DeliveryDAOImpl(), "TABLE Delivery");
        showTable(new Laptop().getColumns(), new LaptopDAOImpl(), "TABLE Laptop");
        showTable(new Order().getColumns(), new OrderDAOImpl(), "TABLE Order");
        showTable(new Password().getColumns(), new PasswordDAOImpl(), "TABLE Password");
        showTable(new Payment().getColumns(), new PaymentDAOImpl(), "TABLE Payment");
        showTable(new Product().getColumns(), new ProductDAOImpl(), "TABLE Product");
        showTable(new ProductHasOrder().getColumns(), new ProductHasOrderDAOImpl(), "TABLE ProductHasOrder");
        showTable(new ProductHasStorage().getColumns(), new ProductHasStorageDAOImpl(), "TABLE ProductHasStorage");
        showTable(new Status().getColumns(), new StatusDAOImpl(), "TABLE Status");
        showTable(new Storage().getColumns(), new StorageDAOImpl(), "TABLE Storage");
        showTable(new Toy().getColumns(), new ToyDAOImpl(), "TABLE Toy");
        showTable(new User().getColumns(), new UserDAOImpl(), "TABLE user");
    }

    private static void showAllTablesStructure() throws SQLException {
        DBStructure.getAllTables().forEach(s -> LOGGER.info(s + "\n"));
    }

    private static void chooseTable() throws SQLException {
        showMenu(tableList, tableListMethods);
    }

    private static void quit() {
        LOGGER.info("OK");
    }

    public void show() throws SQLException {
        showMenu(generalMenu, generalMenuMethods);
    }

    private static void showMenu(Map<String, String> menu, Map<String, Printable> menuMethods) throws SQLException {
        String option;
        do {
            printMenu(menu);
            LOGGER.info("Choose one option: ");
            option = SCANNER.nextLine().toUpperCase().trim();
            if (menuMethods.containsKey(option)) {
                menuMethods.get(option).print();
            } else {
                LOGGER.info("Wrong input! Try again.\n");
            }
        } while (!option.equals("Q"));
    }
}
