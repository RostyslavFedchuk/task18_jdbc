package com.epam.controller;

import com.epam.controller.dao.implementations.CategoryDAOImpl;
import com.epam.model.tables.Category;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

import static junit.framework.TestCase.assertEquals;

public class CategoryDAOImplTest {
    private static final Logger LOGGER = LogManager.getLogger(CategoryDAOImplTest.class);

    @Test
    @DisplayName("Testing insert")
    @RepeatedTest(20)
    public void insertDeleteTest(){
        Category category = new Category(12,"Hello", "");
        try {
            assertEquals(1, new CategoryDAOImpl().create(category));
            LOGGER.info("Added new row into table\n");
        } catch (SQLIntegrityConstraintViolationException e){
            try {
                assertEquals(1, new CategoryDAOImpl().delete(12));
                LOGGER.info("Deleted new row from a table\n");
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
